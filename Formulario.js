function submitForm() {
    var username = document.getElementById('uname').value;
    var password = document.getElementById('psw').value;
    var message = "L'usuari " + username + " ha accedit a la plataforma amb la contrasenya " + password;
    showMessage(message);
}

function cancelOperation() {
    var message = "L'usuari ha cancelat l'operacio.";
    showMessage(message);
}

function showMessage(message) {
    var messageDiv = document.getElementById('messageDiv');
    messageDiv.innerHTML = '<div>' + message + '</div>';
}