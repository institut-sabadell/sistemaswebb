function agregarFila(nombre, imagen) {

    var tabla = document.getElementById('tablaPersonajes');
    var fila = tabla.insertRow();
    var celdaNombre = fila.insertCell(0);
    var celdaImagen = fila.insertCell(1);

    celdaNombre.innerHTML = nombre;
    celdaImagen.innerHTML = '<img src="' + imagen + '" alt="' + nombre + '" class="img-fluid" style="max-width: 200px; max-height: 200px;">';

  }

  agregarFila('Doraemon', 'https://th.bing.com/th/id/OIP.BR6Vjb59xFiBjDWk6WmN0gHaKP?rs=1&pid=ImgDetMain');
  agregarFila('Novita', 'https://th.bing.com/th/id/OIP.Lc7JHMHKExw9V5X41rvKYwHaJj?rs=1&pid=ImgDetMain');
  agregarFila('Gigante', 'https://th.bing.com/th/id/R.83a626bc82954e45eb551d51bb17f40a?rik=oC54wpoiZQKGoQ&riu=http%3a%2f%2f2.bp.blogspot.com%2f-Wr5PkNQojpE%2fVOuNJqR-MGI%2fAAAAAAAAAVU%2fKUY3Wfq60yM%2fs1600%2fgigante.png&ehk=xXec%2f1pWnN5CRKiw%2fOFvanCCSzMIjsGuTxDNTU63CXs%3d&risl=&pid=ImgRaw&r=0');
  agregarFila('Shizuka', 'https://th.bing.com/th/id/OIP.cEaXFz-RbAn89URHLw9G6wAAAA?rs=1&pid=ImgDetMain');
  agregarFila('Suneo', 'https://i.pinimg.com/originals/2c/55/f1/2c55f1b6dcb269f26ed05a04ddf1da3e.jpg');